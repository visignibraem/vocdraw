# VOCDraw

VOCDraw is script, that draws bounding boxes, which are presented in PascalVOC XML annotation format

## Prerequisites
- python3
- python-tk
- imagemagick (option)
## Installation

```bash
git clone https://gitlab.com/visignibraem/vocdraw.git
cd vocdraw
chmod +x vocdraw.py
```

## Usage

Here 3 modes: work with separate file, with folder or as module.

With file:
```bash
./vocdraw abs/path/to/file.xml
```
With file save:
```bash
./vocdraw abs/path/to/file.xml abs/path/to/OutImagesDir
```
With folder:
```bash
./vocdraw $DIR/Annotations $DIR/OutImagesDir
```

As module
```python
from vocdraw import VocDraw

vd = VocDraw() # or  with out folder name in args
vd.parse_xml('/abs/path/to/xml')
vd.bbox()
my_img = vd.img # simple PIL image with bounding boxes
```

## Tips:
- Using imagemagick with folder mode you can show next image with Space button and previous with Backspace.
