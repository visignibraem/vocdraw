#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
    __     __         ____
    \ \   / ___   ___|  _ \ _ __ __ ___      __
     \ \ / / _ \ / __| | | | '__/ _` \ \ /\ / /
      \ V | (_) | (__| |_| | | | (_| |\ V  V /
       \_/ \___/ \___|____/|_|  \__,_| \_/\_/

    VOCDraw script draws bounding boxes using xml files, which are provided in PascalVOC XML annotation format:
    https://gist.github.com/Prasad9/30900b0ef1375cc7385f4d85135fdb44

    about PascalVOC: http://host.robots.ox.ac.uk/pascal/VOC/

    XML parser here find all boxes using 'xmin', 'xmax', 'ymin', 'ymax' nodes in xml.
    Image file founding using 'path' node in xml file.

    Usage:
        with folders: `$python vocdraw.py /abs/path/to/xmldir /abs/path/to/outdir`
        with file: `$python vocdraw.py /abs/path/to/file.xml`
        with file & saving to folder: `$python vocdraw.py /abs/path/to/file.xml /abs/path/to/outdir`

    Usage as module:
            from vocdraw import VocDraw

            vd = VocDraw() # or out folder name in args
            vd.parse_xml('/abs/path/to/xml')
            vd.bbox()
            my_img = vd.img # simple PIL image with bounding boxes.

"""
import argparse
import os
import sys
import subprocess
import xml.etree.ElementTree as ET
from PIL import Image, ImageDraw


class VocDraw:
    def __init__(self, out_folder=None):
        self.out_folder = out_folder

    def parse_xml(self, xml_path):
        tree = ET.parse(xml_path)
        root = tree.getroot()
        self.image_path = root.find('path').text
        self.points = zip(root.iter('xmax'), root.iter('ymax'), root.iter('xmin'), root.iter('ymin'))

    def draw_bbox(self):
        self.img = Image.open(self.image_path)
        draw = ImageDraw.Draw(self.img)
        for point in self.points:
            point = map(lambda x: float(x.text), point)
            draw.rectangle([*point], outline="red")

    def save(self):
        if not os.path.exists(self.out_folder):
            os.mkdir(self.out_folder)
        self.img.save(os.path.join(self.out_folder,os.path.basename(self.image_path)))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="This script draws bounding boxes using xml files, which are \
                                                  provided in PascalVOC XML annotation format")
    parser.add_argument("in_path", help="abs path to xmlfile or folder with xml annotations", type=str)
    parser.add_argument("out_folder", nargs='?', help="abs path to out folder (required if in_path is folder). \
                         If not empty in one xml file mode create a file by dest. path", type=str)
    args = parser.parse_args()

    vd = VocDraw(args.out_folder)
    if os.path.basename(args.in_path).endswith('.xml'):   # 1 file mode

        vd.parse_xml(args.in_path)
        vd.draw_bbox()
        vd.img.show()
        if args.out_folder:
            vd.save()
        else:
            print('Show only!')

    else:                                                 # folder mode
        if not args.out_folder:
            raise ValueError("In folder mode out path required! Help: 'vocdraw -h'")

        xml_files = os.listdir(args.in_path)
        for i, xml_file in enumerate(xml_files):
            sys.stdout.write('\r{}/{}'.format(i+1, len(xml_files)))
            sys.stdout.flush()

            vd.parse_xml(os.path.join(args.in_path, xml_file))
            vd.draw_bbox()
            vd.save()

        print('\nDone!')
        try:
            print('Opening ImageMagick...')
            subprocess.call(['display', args.out_folder+'/*'])
        except FileNotFoundError:
            print('ImageMagick not found. You can install it: "sudo apt-get install imagemagick"')

